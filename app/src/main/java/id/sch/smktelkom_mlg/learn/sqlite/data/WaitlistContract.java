package id.sch.smktelkom_mlg.learn.sqlite.data;

import android.provider.BaseColumns;

/**
 * Created by SMK TELKOM on 2/27/2018.
 */

public class WaitlistContract {
    public static final class WaitlistEntry implements BaseColumns {
        public static final String TABLE_NAME = "waitlist";
        public static final String COLUMN_GUEST_NAME = "guestName";
        public static final String COLUMN_PARTY_SIZE = "partySize";
        public static final String COLUMN_TIMESTAMP = "timestamp";
    }
}
